import transposition
import os
import sys
import datetime
import time

def encrypt(inFile, outFile, key=1):
	''' Encrypt file '''
	INPUT_FILENAME = inFile
	OUTPUT_FILENAME = outFile
	KEY = key

	if not os.path.exists(INPUT_FILENAME):
		print "%s does not exist" % INPUT_FILENAME
		sys.exit(1)

	# if os.path.exists(OUTPUT_FILENAME):
	# 	print "%s will be overwriten? Yes/No" % OUTPUT_FILENAME
	# 	response = raw_input().lower()
	# 	if response[0] != 'y':
	# 		print "Aborting..."
	# 		sys.exit(1)

	inputFile = open(INPUT_FILENAME)
	data = inputFile.read()
	inputFile.close()
	size = len(data)

	startTime = datetime.datetime.now()
	
	encryptedData = transposition.encrypt(data, KEY)
	outputFile = open(OUTPUT_FILENAME, 'w')
	outputFile.write(encryptedData)
	outputFile.close()

	totalTime = datetime.datetime.now() - startTime

	print "Encrypted %d bytes in %d:%d seconds" % (size, totalTime.seconds, totalTime.microseconds)

def decrypt(inFile, outFile, key=1):
	''' Decrypt file '''
	INPUT_FILENAME = inFile
	OUTPUT_FILENAME = outFile
	KEY = key

	if not os.path.exists(INPUT_FILENAME):
		print "%s does not exist" % INPUT_FILENAME
		sys.exit(1)

	# if os.path.exists(OUTPUT_FILENAME):
	# 	print "%s will be overwriten? Yes/No" % OUTPUT_FILENAME
	# 	response = raw_input().lower()
	# 	if response[0] != 'y':
	# 		print "Aborting..."
	# 		sys.exit(1)

	data = open(INPUT_FILENAME).read()
	size = len(data)

	startTime = datetime.datetime.now()
	
	decryptedData = transposition.decrypt(data, KEY)
	outputFile = open(OUTPUT_FILENAME, 'w')
	outputFile.write(decryptedData)
	outputFile.close()

	totalTime = datetime.datetime.now() - startTime

	print "Decrypted %d bytes in %d:%d seconds" % (size, totalTime.seconds, totalTime.microseconds)

def main():
	encrypt('frankenstein.txt', 'frankenstein_encrypted.txt', 33)
	decrypt('frankenstein_encrypted.txt', 'frankenstein_decrypted.txt', 33)

	encrypt('thetimemachine.txt', 'thetimemachine_encrypted.txt', 33)
	decrypt('thetimemachine_encrypted.txt', 'thetimemachine_decrypted.txt', 33)

if __name__ == '__main__':
	main()