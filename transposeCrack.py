import transposeFile
import transposition
import os
import datetime
import sys
import random

INPUT_FILE = 'frankenstein.txt'
ENCRYPTED_FILE = 'frankenstein_encrypted.txt'
DECRYPTED_FILE = 'frankenstein_decrypted.txt'

CONTINUE = True # change to True to continue previous session
CONTINUE_KEY = 3400 # replace with the last key from the previous session

def isEnglish(testData, dictionary, percent = 20):
	startTime = datetime.datetime.now() # check time

	data = testData.split(' ')
	totalWords = len(data)
	englishWords = 0

	for word in data:
		if word.upper() in dictionary:
			englishWords += 1

	totalTime = datetime.datetime.now() - startTime
	minutes = totalTime.seconds / 60
	seconds = totalTime.seconds % 60
	print "Processed in %d minute(s) %d second(s) %d millisecond(s)" % (minutes, seconds, totalTime.microseconds)

	print "English words found: %d out of %d" % (englishWords, totalWords)
	englishWordPercentage = englishWords / float(totalWords) * 100
	if englishWordPercentage >= percent:
		print "English content esimation: %d%%. Threashhold is %d%%" % (englishWordPercentage, percent)
		return True

	print "English content esimation: %d%%. Threashhold is %d%%" % (englishWordPercentage, percent)
	return False

def main():

	wordData = open('dictionary.dict')
	words = {} # test later to compare execution time
	#words = wordData.read().split('\n')
	#wordData.close()

	for word in wordData.read().split('\n'):
		words[word] = None

	inputFile = open(INPUT_FILE)
	inputData = inputFile.read()
	inputFile.close()
	inputSize = len(inputData)
	del inputData # free RAM

	# do not overwrite file if we continue session to avoid corruption of the old key
	if not CONTINUE:
		# encrypt and save for cracking tests
		# do not use key larger than 30% of the total data lengths
		# otherwise big chunks of text will not be encrypted due to rolling over 
		transposeFile.encrypt(INPUT_FILE, ENCRYPTED_FILE, random.randint(0,inputSize-1)/3) # debug key

	inputEncryptedFile = open(ENCRYPTED_FILE)
	encryptedData = inputEncryptedFile.read()
	inputEncryptedFile.close()

	print "-- BEGIN CRACKING --"
	startTime = datetime.datetime.now() # check time
	if not CONTINUE:
		startingKey = 0
	else:
		startingKey = CONTINUE_KEY

	for key in range (startingKey, inputSize):
		if key % 1 == 0:
			print "Key: %d. Left: %d" % (key, inputSize-key)
			totalTime = datetime.datetime.now() - startTime
			minutes = totalTime.seconds / 60
			if minutes >= 60:
				minutes = minutes % 60
			seconds = totalTime.seconds % 60
			hours = totalTime.seconds / 60 / 60
			days = totalTime.days
			print "Processed in %d day(s) %d hour(s) %d minute(s) %d second(s) %d millisecond(s)" \
				% (days, hours, minutes, seconds, totalTime.microseconds)

		decryptedData =  transposition.decrypt(encryptedData, key)
		print "Checking for English..."
		if isEnglish(decryptedData, words):
			totalTime = datetime.datetime.now() - startTime
			minutes = totalTime.seconds / 60
			if minutes >= 60:
				minutes = minutes % 60
			seconds = totalTime.seconds % 60
			hours = totalTime.seconds / 60 / 60
			days = totalTime.days
			print "Processed in %d day(s) %d hour(s) %d minute(s) %d second(s) %d millisecond(s)" \
				% (days, hours, minutes, seconds, totalTime.microseconds)
			print "Possible key found: %d: %s" % (key, decryptedData[0:100])
			if raw_input('Correct? Yes/No ').strip().lower().startswith('y'):
				transposeFile.decrypt(ENCRYPTED_FILE, DECRYPTED_FILE, key)
				print "Decrypted file created."
				sys.exit(0)
			else:
				print "Cracking..."
		else:
			print "Not English."
	print "Key not found" # failed to crack
	totalTime = datetime.datetime.now() - startTime
	minutes = totalTime.seconds / 60
	seconds = totalTime.seconds % 60
	hours = minutes / 60
	days = totalTime.days
	print "Processed in %d day(s) %d hour(s) %d minute(s) %d second(s) %d millisecond(s)" % \
		(days, hours, minutes, seconds, totalTime.microseconds)

if __name__ == '__main__':
	main()