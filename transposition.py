def encrypt(input, key):
	""" Transposition encrypt: input - string. key - numeric key """
	outputString = ''

	if key == 0 or key == 1:
		return input

	for step in range(0, key):
		for i in range(step, len(input), key):
			outputString += input[i]

	return outputString		

def decrypt(input, key):
	""" Transposition dencrypt: input - string. key - numeric key """
	outputString = [None]*len(input)

	if key == 0 or key == 1:
		return input

	k = 0
	for step in range(0, key):
		for i in range(step, len(input), key):
			outputString[i] = input[k]
			k += 1

	return ''.join(outputString)		

def main():
	""" Some tests of the functions above. """
	#test1 = 'Common sense is not so common.'
	test1 =  'dramatic'
	key1 = 1
	encrypted1 = encrypt(test1, key1)
	print "Original: %s\nKey:%d\nEncrypted: %s" % (test1, key1, encrypted1)
	print"\n"
	decrypted1 = decrypt(encrypted1, key1)
	print "Original: %s\nKey:%d\nDecrypted: %s" % (encrypted1, key1, decrypted1)
	print '-'*20

	test2 = "H cb  irhdeuousBdi   prrtyevdgp nir  eerit eatoreechadihf paken ge b te dih aoa.da tts tn"
	test3 = "A b  drottthawa nwar eci t nlel ktShw leec,hheat .na  e soogmah a  ateniAcgakh dmnor  "
	test4 = "Bmmsrl dpnaua!toeboo'ktn uknrwos. yaregonr w nd,tu  oiady hgtRwt   A hhanhhasthtev  e t e  eo"
	print decrypt(test2, 9)
	print decrypt(test3, 9)
	print decrypt(test4, 9)

if __name__ == '__main__':
	main()
