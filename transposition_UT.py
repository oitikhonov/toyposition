import transposition
import random
import sys
import string

NUMBER_OF_TESTS = 50
MATERIAL = string.ascii_uppercase + string.ascii_lowercase + string.digits
#print "MATERIAL size: ", len(MATERIAL)

for test in range(0,NUMBER_OF_TESTS):
	testString = ''
	
	size = random.randint(0,50)
	for i in range(0, size+1):
		testString += MATERIAL[random.randint(0, len(MATERIAL)-1)]

	key = random.randint(0, len(testString))
	encrypted = transposition.encrypt(testString, key)
	decrypted = transposition.decrypt(encrypted, key)

	if testString == decrypted:
		#pass
		print "Test #%d PASS key:%d Original:%s Encrypted:%s" % (test, key, testString, encrypted)
	else:
		print "Test #%d ERROR key:%d Original:%s Encrypted:%s Decrypted:%s" % (test, key, testString, encrypted, decrypted)